using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateFrame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    //update的升级版，但很少用。区别在于其执行频率不再随着电脑的帧率而改变，而是有一个人设的固定值
    private void FixedUpdate()
    {
        Debug.Log("当前帧率(FPS)："+(1/Time.deltaTime));
    }
}
