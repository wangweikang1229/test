using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject firebomb;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //当且仅当鼠标左键按下时才会创建子弹
        if (Input.GetMouseButtonDown(0))
        {
            // 创建预制件firebomb的实例，并以camera的位置和角度为基准
            GameObject bullet = GameObject.Instantiate(firebomb, transform.position, transform.rotation);
            // 获取所建实例的刚体
            Rigidbody body = bullet.GetComponent<Rigidbody>();
            // // 向当前刚体施加力，但这种方法不够直观
            // body.AddForce(Vector3.forward*2000);
            body.velocity = Vector3.forward*30;//给子弹一个初速度。
        }
    }
}