using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public int speed = 15;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }

    void move()
    {
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
        {
            //turn(45);
            force(1, 1);
        }

        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
        {
            //turn(-45);
            force(-1, 1);
        }

        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
        {
            //turn(135);
            force(1, -1);
        }

        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
        {
            //turn(-135);
            force(-1, -1);
        }
        else
        {
            if (Input.GetKey(KeyCode.W))
            {
                //turn(0);
                force(0, 1);
            }

            if (Input.GetKey(KeyCode.A))
            {
                //turn(-90);
                force(-1, 0);
            }

            if (Input.GetKey(KeyCode.S))
            {
                //turn(180);
                force(0, -1);
            }

            if (Input.GetKey(KeyCode.D))
            {
                //turn(90);
                force(1, 0);
            }

            if (Input.GetKey(KeyCode.Space))
            {
                transform.Translate(new Vector3(0, 1, 0) * speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.LeftAlt))
            {
                transform.Translate(new Vector3(0, -1, 0) * speed * Time.deltaTime);
            }
        }
    }

    // void turn(int angel)
    // {
    //     transform.localRotation = Quaternion.Euler(0, angel, 0);
    // }

    void force(int x, int z)
    {
        transform.Translate(new Vector3(x, 0, z) * speed * Time.deltaTime);
    }
}