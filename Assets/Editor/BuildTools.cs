using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;

public class BuildTools
{
    [MenuItem("Build/Build EXE")]
    public static void BuildEXE()
    {
        // 解析命令行参数
        string[] args = System.Environment.GetCommandLineArgs();
        foreach (var s in args)
        {
            // 设置游戏名字
            if (s.Contains("--productName:"))
            {
                string productName = s.Split(':')[1];
                PlayerSettings.productName = productName;
            }

            // 设置版本号
            if (s.Contains("--version:"))
            {
                string version = s.Split(':')[1];
                PlayerSettings.bundleVersion = version;
            }
        }

        //开启打包流程
        BuildPlayerOptions opt = new BuildPlayerOptions(); //打包方法的大统领
        opt.scenes = new string[] {"Assets/Scenes/SampleScene.unity"}; //打包哪个场景？
        opt.locationPathName = Application.dataPath + $"/../Bin/{PlayerSettings.productName}_{PlayerSettings.bundleVersion}/{PlayerSettings.productName}.exe"; //打包到哪里？
        opt.target = BuildTarget.StandaloneWindows64; //打包到什么平台？
        opt.options = BuildOptions.None; //其他打包操作，比如是否运行构建的播放器？
        BuildReport report = BuildPipeline.BuildPlayer(opt); //有关 Unity 构建过程的信息
        BuildSummary summary = report.summary; //包含有关打包的总体摘要信息
        if (summary.result == BuildResult.Succeeded)
        {
            float exeSize = summary.totalSize / 1024 / 1024;
            Debug.Log($"打包成功！可喜可贺！一共{exeSize}M！");
        }
        else if (summary.result == BuildResult.Failed)
        {
            Debug.Log($"打包失败？！去看看发生什么事了！");
        }
    }
}